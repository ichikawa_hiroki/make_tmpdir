#!/bin/bash




tmp_dir=~/tmp/`date +%y%m%d`

sidebar_script=$(cd $(dirname ${BASH_SOURCE:-$0}); pwd)
sidebar_script+="/set_tmpdir_to_sidebar.applescript"

# make tmp directory
if [ ! -e ~/tmp ]; then
        mkdir ~/tmp
fi

# make today's directory
if [ ! -e $tmp_dir ]; then
        mkdir "$tmp_dir"
	if [ -L ~/tmp/todays_tmp ]; then
                unlink ~/tmp/todays_tmp
        fi
	ln -nfs "$tmp_dir" ~/tmp/todays_tmp
        /usr/bin/osascript "$sidebar_script"
fi

cd "$tmp_dir"
